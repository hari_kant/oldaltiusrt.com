$(function() {
  $('a.hash[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);

      }
    }
  });
});


$(document).ready(function() {

$('#shareme').sharrre({
	share: {
		twitter: true,
		facebook: true,
		googlePlus: true,
		linkedin: true
	},
	template: '<div class="box"><div class="left">Share</div><div class="middle"><a href="#" class="facebook">f</a><a href="#" class="twitter">t</a><a href="#" class="googleplus">+1</a><a href="#" class="li">in</a></div></div>',
	enableHover: false,
	urlCurl:'',

	render: function(api, options){
		$(api.element).on('click', '.twitter', function() {
			api.openPopup('twitter');
		});
		$(api.element).on('click', '.facebook', function() {
			api.openPopup('facebook');
		});
		$(api.element).on('click', '.googleplus', function() {
			api.openPopup('googlePlus');
		});
		$(api.element).on('click', '.li', function() {
			api.openPopup('linkedin');
		});
	}
	});
});

(function() {
	$('form[data-remote]').on('submit',function(e) {

		var form=$(this);

		var method=form.find('input[name="_mothod"]').val() || 'POST';
		var url = form.prop('action');


		$.ajax({
			type: method,
			url: url,
			data: form.serialize(),
			success: function() {
//				_trackEvent('Forms','Submit','Contact Form');
//				_gaq.push(['_trackEvent'],'Forms','Submit','Contact Form');
		ga('send','Forms','Submit','Contact Form');
				var div=$('#form');

			
				div.fadeOut('slow', function() {
						div
						.html('<h3>Thanks for your interest in <span class="altiusrt">Altius<span>rt</span></span></h3><p>We will be in touch shortly.</p>')
						.fadeIn('slow');
					
					
				});



			}
		});


		e.preventDefault();
	});


	$('input[data-confirm],button[data-confirm]').on('click',function(e) {
		var input=$(this);
		var form = input.closest('form');

		if(! confirm(input.data('confirm'))) {
			e.preventDefault();
		}



	});






}) ();