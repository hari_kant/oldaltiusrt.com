<?php
$var=
	[	'load'		=>  sys_getloadavg() ,
		'hostname'  =>  gethostname() ,
		'phpversion'=>  phpversion(),
		'linux'		=> php_uname('s').' '.php_uname('r')

		];
	$mi = getSystemMemInfo();
	
	$save = ['MemTotal','MemFree'];

	foreach($save as $s)
	$var[$s]=round(intval($mi[$s])/1024);




echo( json_encode($var));

function getSystemMemInfo() 
{       
    $data = explode("\n", trim(file_get_contents("/proc/meminfo")));
    $meminfo = array();
    foreach ($data as $line) {

        list($key, $val) = 
			explode(":", $line);
        $meminfo[$key] = trim($val);
    }
    return $meminfo;
}