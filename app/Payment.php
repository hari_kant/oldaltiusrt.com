<?php

namespace App;

class Payment extends \Eloquent {


	public function getCentsAttribute($v) {
		return $this->amount*100;

	}
	public function getDescriptionAttribute($v) {
		return sprintf('%s (%.2f %s)',$this->name,$this->amount,$this->currency);

	}
}
