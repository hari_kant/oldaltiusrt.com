<?php namespace App\Http\Controllers;

class PagesController extends BaseController {

	public function home()
	{
		$ago = date('s')%60 + 1;

		if($ago==1)
			$text='as of 1 second ago';
		else
			$text=sprintf('as of %d seconds ago',$ago);


		$text='updates in realtime';
		$stats=$this->getStats();

//		$stats = \Cache::remember('altiusrtstats',0,function() {
//			return $this->getStats();
//		});

		return \View::make('pages.home.index',['stats' => $stats,'testimonials' => $this->getTestimonials(),'ago'=>$text]);

	}
	public function terms() {
		return \View::make('pages.terms');
	}
	public function fih2014stats() {
		return \View::make('pages.stats.fih2014');
	}
	public function form() {
//		!d(\Input::all());
		
		$rules= [
				'fname' => 'required',
				'lname' => 'required',
				'email' => 'required|email',
				'organization' => 'required',
//				'title' => 'required',

				'country' => 'required',
			    'instructions'   => 'honeypot',
			    'objective'   => 'required|honeytime:5'

				];


		$validator = \Validator::make(\Input::all(),			$rules);

		if($validator->fails()) {
			return \Redirect::to('/#contact')->withErrors($validator)->withInput();

		}

		\Log::info('Form Submission',\Input::all());

		\Mail::send('emails.contact', [], function($message)
		{
			$message->to('ops@altiusrt.com', 'Hari')->subject('Contact us form submission!!')
				->from('hari@altiusrt.com','Altius');

		});

		\Mail::send('emails.verification', [], function($message)
		{
			$message  ->to(\Input::get('email'), \Input::get('fname') .' ' . \Input::get('lname'))
					  ->subject('Thank you for interest in Altiusrt')
					  ->from('hari@altiusrt.com','Hari Kant');

		});

		return \Redirect::to('/#contact')->with('success',1);

	}
	public function getTestimonials() {


		$ret=[];
		$ret[] =	[
						'quote'		=> 'A more professional presentation of our events, freeing up at least one volunteer for our hosts. Building knowledge of our athletes to have statistical information about them, which will ultimately be useful for our sport and building heros for hockey.',
						'person'	=> 'Siobhan Madeley, European Hockey Federation',
						'img_large'	=> '/images/testimonials/ehf.png',
						'img_small'	=> '/images/testimonials/siobhan_madeley.jpg',
					];

		$ret[] =	[
						'quote'		=> '<i>Altiusrt</i> makes life easy for the technical officials. All statistic reports like cards, goal scorers, and pool standings are produced automatically and are available at once. It isn\'t necessary anymore to hand starting line-ups and match reports over to the teams, press etc, because everyone can see and work with these reports online',	
						'person'	=> 'Christian Deckenbrock, DHB, Technical Officer 2015 FIH Indoor World Cup',
						'img_large'	=> '/images/testimonials/germany.png',
						'img_small'	=> '/images/testimonials/christian_deckenbrock.png',
					];
		return $ret;
		$faker = Faker\Factory::create();
		$ret []= 
				[	
					'quote'			=> 'The most positive part was how responsive our people on the bench were and how quickly they picked up the processes and running of the match sheets.',
					'person'		=> 'Wayne Sleeman, Hockey New Zealand',
					'img_large'		=> '/images/testimonials/new_zealand.png',
					'img_small'		=> '/images/testimonials/wayne_sleeman.png',
				];

		$ret[] =	[
						'quote'		=> 'Great system; easy to use. I would certainly support its expanded use by HA at all National Championships. It was an outstanding success in Sydney for the U21 Mens',
						'person'	=> 'Josh Burt, Hockey Australia',
						'img_large'	=> '/images/testimonials/20140615M_38_AUSvsNED_SB 4.jpg',
						'img_small'	=> '/images/testimonials/20140615M_38_AUSvsNED_SB 4.jpg',
					];




		foreach(range(1,1) as $i) {
			$ret[]= 
					[
						'quote'		=> $faker->paragraph(3),
						'person'	=> $faker->name .', ' . $faker->company,
						'img_large'	=> $faker->imageUrl(400,400,'people'),
						'img_small'	=> $faker->imageUrl(200,200,'people'),

						];

		}
		return $ret;
	}

	public function test() {
		!d($this->getStats());
		exit;

	}
	public function getStats() {

		$totals=[ 
			'matches'=>0,
			'athletes'=>0,
			'goals'=>0,
			];


		$tenants = \DB::connection('master')->select('select * from tenants');


		foreach($tenants as $t) {

			\DB::disconnect('tenant');
			\Config::set('database.connections.tenant.schema',strtolower($t->folder));			

			try {

				$count=\DB::connection('tenant')->select("select count(*) as count from matches where status='Official'")[0]->count;
				$totals['matches']+=$count;



				$count=\DB::connection('tenant')->select("select count(*) as count from people where is_athlete")[0]->count;
				$totals['athletes']+=$count;

				$count=\DB::connection('tenant')->select("select sum(homescore +awayscore ) as count from matches where status='Official'")[0]->count;
				$totals['goals']+=$count;
			} catch (\Exception $e) { }
		}

		return $totals;
	}

}
