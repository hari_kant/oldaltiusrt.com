<?php namespace App\Http\Controllers;

use \App\Payment;

class PaymentController extends BaseController {

	public function payment($slug) {


		$payment=Payment::where('name',urldecode($slug)) 
				  ->first();

		if($payment) {
			return \View::make('payment.pay',['payment'=>$payment]);	
		}
		\App::abort(404);
	}


	public function paymentPost($slug) {
		$payment=Payment::where('name',urldecode($slug)) 
				  ->first();
		if(!$payment) {
			return Redirect::back()->with('error','We cannot find this payment');
		}
		if($payment->status!='New')
			return Redirect::back()->with('error','This payment has already been made');
		
		\Stripe\Stripe::setApiKey(\Config::get('services.stripe.sk_live'));


		// verify that it is new


		// Get the credit card details submitted by the form
		$token = \Input::get('stripeToken');

		// Create the charge on Stripe's servers - this will charge the user's card
		try {
		  $charge = \Stripe\Charge::create(array(
			"amount" => $payment->cents, // amount in cents, again
			"currency" => $payment->currency,
			"source" => $token,
			"metadata" => $payment->toArray(),
			"description" => $payment->description

			));
		} catch(\Stripe\Error\Card $e) {
				  // Since it's a decline, \Stripe\Error\Card will be caught
				  $body = $e->getJsonBody();
				  $err  = $body['error'];
				return \Redirect::back()->with('error',$err['message']);
		} catch (\Exception $e) {
			return \Redirect::back()->with('error',$e->getMessage());

		}


		$payment->stripe_id=$charge->id;
		$payment->status="Approved";
		$payment->save();

		$this->email($payment);


		return \Redirect::back()->with('success','We have successfully charged your card');


	}
	protected function email($payment) {
		\Mail::send('payment.email', ['payment'=>$payment], function($message) use($payment)
		{
			$message->to('ops@altiusrt.com', 'Hari')->subject('Payment: ' . $payment->description)
				->from('hari@altiusrt.com','Altius');
		});



	}
}
