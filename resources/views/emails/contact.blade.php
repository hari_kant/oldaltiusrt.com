<h3>Information from AltiusRT contact form:</h3>
<table>
	<tr>
		<th>First Name
		</th>
		<td>{{ \Input::get('fname') }}
		</td>
	</tr>
	<tr>
		<th>Last Name
		</th>
		<td>{{ \Input::get('lname') }}
		</td>
	</tr>
	<tr>
		<th>Email
		</th>
		<td>{{ \Input::get('email') }}
		</td>
	</tr>
	<tr>
		<th>Organization
		</th>
		<td>{{ \Input::get('organization') }}
		</td>
	</tr>
	{{--
	<tr>
		<th>Title
		</th>
		<td>{{ \Input::get('title') }}
		</td>
	</tr>
--}}
	<tr>
		<th>Country
		</th>
		<td>{{ \Input::get('country') }}
		</td>
	</tr>
	<tr>
		<th>Message
		</th>
		<td>{{ \Input::get('message') }}
		</td>
	</tr>
</table>