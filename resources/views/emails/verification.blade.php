@extends('emails.layout')
@section('content')
<br />
<h3>Dear {{{ \Input::get('fname') }}} {{{ \Input::get('lname') }}},</h3>
<br />
<p>Thanks for your interest in <span class="altiusrt">Altius<span>rt</span></span>.</p>

<p >My name is Hari Kant and I work at <span class="altiusrt">Altius<span>rt</span></span>.  I will be in touch shortly to learn more about your sport stats requirements. </p>
<br />
<p>In the meantime please think about the following questions:
<ul>
	<li>How many competitions do you typically run in a year?</li>
	<li>How many games is that?</li>
	<li>How are you keeping track of your athletes and officials?</li>
	<li>How do you currently showcase live results to the public?</li>
	<li>What are your key pain points in managing your competitions?</li>
	<li>What are your key pain points in getting your results to the public?</li>
</ul>
</p>

<p>We will be in touch soon.</p>
<br />
<p>Best Regards,</p>

<p>Hari Kant<br>
<span class="altiusrt">Altius<span>rt</span></span>
</p>
<br />

@stop