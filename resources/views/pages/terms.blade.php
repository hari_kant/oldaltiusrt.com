@extends('layouts.default')
@section('content')

	@include('pages.home.navigation')
	<div class="container">

<h3>Terms and Conditions</h3>

<p>By viewing and using TeleAccess Inc. applications, services, platforms, profiles or the TeleAccess website (www.altiusrt.com) (together, the application) you are deemed to agree to these terms and conditions of use without qualification. If you do not agree to be bound by these terms and conditions, you must stop accessing and using our application. We reserve the right to change these terms and conditions of use at any time by notifying users of the existence of the amended terms and conditions through our application. By continuing to access our application, you agree to be bound by the amended terms and conditions.
</p>

<h4>Personal information</h4>

<p>In order to access and use certain content, features and services we may require that you register on the application. You may be asked to provide personal information in order to register on our application and to use the services. You consent to the release of your personal information to us and to third parties in connection with TeleAccess Inc. If you have questions or comments about the Terms and Conditions, please contact us at: info@altiusrt.com
</p>

<p>By registering on our application, you consent to receiving marketing, promotional and other material by way of electronic messages from us. You will ensure that all usernames and passwords required to access our application are kept secure and confidential and you will notify us immediately of any unauthorised use of your password or any other breach of security. You must not transfer your account to another user or maintain more than one account with us without our consent. We may, at our sole discretion, suspend or terminate your account and limit your access to our application.
</p>

<h4>Users outside Canada</h4>

<p>Our application is operated by TeleAccess Inc. from Canada. The information on our application may not be appropriate or available for use in other jurisdictions. If you choose to access our application from a jurisdiction other than Canada, you do so on your own initiative and you are responsible for compliance with any applicable laws of that jurisdiction.
</p>

<h4>Copyright and trade marks</h4>

<p>The contents of our application are the copyright of TeleAccess Inc., its related companies or suppliers to us. No part of our application may be distributed or copied for any commercial purpose and you are not permitted to incorporate the material or any part of it in any other work or publication (whether in hard copy, electronic or any other form) without our prior written consent. You may not frame any part of our application material by including advertising or other revenue generating material. Further, you may not remove or alter any trademarks or logos that appear on any material on our application.
</p>

<h4>Linking</h4>

<p>The contents of our application may include links to third party materials. We will not be responsible for the contents of any linked sites or be liable for any direct or indirect loss or damage suffered by you from accessing, using, relying on or trading with third parties. The linked sites are provided to you only as a convenience, and the inclusion of any linked site does not imply any endorsement of it by us or any association with its operators. We reserve the right to prohibit links to our application and you agree to remove or cease any link upon our request.
</p>

<h4>Advertising</h4>

<p>Any dealings with any advertiser appearing on our application are solely between you and the advertiser or other third party. We are not responsible or liable for any part of any such dealings or promotions.
</p>

<h4>Compliance with laws</h4>

<p>You agree to comply with all laws, regulations, contracts or otherwise in connection with or related to, directly or indirectly, your use of the application.
</p>

<h4>Exclusion of liability</h4>

<p>To the fullest extent permitted by law, we exclude all liability in relation to our application including all express and implied warranties and representations. We will not be responsible for errors or misstatements or be liable, whether in contract, tort (including negligence) or otherwise, for any loss or damage however caused (including indirect, consequential or special loss or damage, or loss of profits, loss of data, loss of anticipated savings or loss of opportunity).
</p>

<h4>Proprietary rights</h4>

<p>We do not claim any ownership rights in any content you submit to the application, including information such as sport statistics and information submitted through the application, any photographs, videos, articles or other content, and all related intellectual property rights (together, the content). You continue to retain any such rights you may have in the content, however, you hereby grant to us a perpetual, irrevocable, fully-paid and royalty-free, sublicensable, transferable and worldwide right to use, modify, delete from, add to, combine with other data, publicly perform, publicly display, reproduce, transmit, sell, distribute, and otherwise exploit such content by all means and manners now or later known. You understand that you will not receive any fees, sums, consideration or remuneration for any of the rights granted herein. If you are a minor, you represent and warrant that your parent or legal guardian has consented to you granting the rights herein.
</p>

<p>Each time you submit content to, or in connection with, the application, you represent and warrant that you are at least the age of majority in the jurisdiction in which you reside or are the parent or legal guardian, or have all proper consents from the parent or legal guardian, of any minor who is depicted in or contributed to any content you submitted to, or in connection with, the application and that, as to the content (i) you are the sole author and/or owner of the content inputted by you on, or in connection with, the application or otherwise have the lawful right to grant the license herein, all without any obligation on us to obtain consent of any third party and without creating any obligation or liability on us, (ii) the content is accurate, (iii) the inputting of your content on, or in connection with the application does not violate the privacy rights, publicity rights, intellectual property rights, contract rights or any other rights of any person or entity, and (iv) the content will not violate this agreement or cause injury or harm to any person or entity. You agree to pay all royalties, fees, and any other monies owing to any person or entity by reason of the use of any content submitted by you on, or in connection with, the application.
</p>

<p>You agree that (i) your content will be treated as non-confidential and will not be returned, and (ii) we do not assume any obligation of any kind to you or any third party with respect to your content. Upon our request, you will furnish us with any documentation necessary to substantiate the rights to such content and to verify your compliance with this agreement. You acknowledge that the internet and the technology of the application, and third parties used to enable it, may be subject to breaches of security and that you are aware that the submission of content may not be secure, and you will consider this before submitting content on, or in connection with, the application.
</p>

<p>The application contains content of other users (User Content). Except as otherwise provided in this agreement, you may not copy, download, communicate, make available, modify, translate, publish, broadcast, transmit, distribute, perform, display, sell or otherwise use any User Content appearing on, or in connection with, the application.
</p>

<p>The application may contain content of third party licensors that are not users (Third Party Licensor). Third Party Licensor content is protected by intellectual property rights and other laws, and each Third Party Licensor retains rights in its Third Party Licensor content. You are granted a limited, revocable, non-sublicensable licence to view, or listen to, as applicable, any Third Party Licensor content solely for your personal, non-commercial use in connection with viewing and using the application and in connection with standard search engine activity or use of standard internet browsers (e.g., for making cache copies). Except for the foregoing licence, and except as otherwise expressly provided in writing by us, you are granted no right, title or interest in any Third Party Licensor content. Except as otherwise provided in this agreement, you may not copy, download, communicate, make available, modify, translate, publish, broadcast, transmit, distribute, perform, display, sell or otherwise use any Third Party Licensor content (except as may be a result of any standard search engine activity or use of a standard internet browser).
</p>

<p>This agreement includes only narrow, limited grants of rights to view application content and to use and access the application. No right or licence may be construed under any legal theory, by implication, estoppel, industry custom or otherwise. All rights not expressly granted to you are reserved by us and our licensors and other third parties. Any goodwill that is created in connection with your use of the application inures to us. Any unauthorised use of the application or application content is prohibited.
</p>

<h4>Indemnity</h4>

<p>You will take all necessary action to defend and indemnify TeleAccess Inc. and any related company of TeleAccess Inc. and their officers and employees against all costs, expenses and damages incurred in connection with any claim brought by a third party arising from a breach by you of any of these terms and conditions.
</p>

{{--
<h4>Entire agreement</h4>

<p>These terms and conditions form the entire agreement between us and you relating to our application and your use of the application.
</p>
--}}

<h4>General provisions</h4>

<p>If at any time we do not enforce any of these terms or conditions or grant you time or other indulgence, we will not be construed as having waived that term or condition or our rights to later enforce that or any other term or condition. Further, if any part or provision of these terms and conditions is deemed to be invalid, unenforceable or in conflict with the law, that part or provision is replaced with a provision which, as far as possible, accomplishes the original purpose of that part of provision. The remaining terms and conditions will be binding on the parties. These terms and conditions are governed by and will be construed in accordance with the laws of Canada and you submit to the exclusive jurisdiction of the Courts of Canada.
</p>


</div>

	@include('pages.home.footer')




@stop