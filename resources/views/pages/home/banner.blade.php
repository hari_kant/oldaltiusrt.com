    <!-- Carousel
    ================================================== -->


	<a name="top" style="position:relative; top: -100px"></a>



<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
	    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li> 
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="/images/banner_blue.jpg" alt="capture and showcase your realtime sports statistics">
		  <div class="social">
			<a class="facebook" href='https://www.facebook.com/pages/Altius-RT/815599395157226?ref_type=bookmark' target="_blank"><i class="fa fa-facebook-square"></i></a>
			<a class="twitter" href='https://twitter.com/AltiusRT' target="_blank"><i class="fa fa-twitter"></i></a>
			<div id="shareme" data-url="http://www.altiusrt.com" data-text="Altiusrt creates web applications to capture and showcase realtime sports stats around the world, allowing clients to save hours of set up time, instantaneously capture official stats and display stats fans want to see."></div>
		   </div>	
        </div>

        <div class="item">
          <img src="/images/banner_green.jpg" alt="realtime sports around the world">
        </div>


      </div>

      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	  {{--        <span class="glyphicon glyphicon-chevron-left"></span>  --}}
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	  {{--                <span class="glyphicon glyphicon-chevron-right"></span>    --}}
      </a>
</div>

<div>
<hr>
</div>

<div id="example2">

</div>
