@extends('layouts.default')

@section('content')
{{-- @include('pages.home.sizeinfo') --}}




	@include('pages.home.navigation')


	@include('pages.home.banner')
	@include('pages.home.product')
	@include('pages.home.proposition')
	@include('pages.home.cta')

	@include('pages.home.testimonials')

	@include('pages.home.platform')
	@include('pages.home.about')
	@include('pages.home.realtime_map')
{{-- 	@include('pages.home.form')
 --}}
	@include('pages.home.footer')




@stop