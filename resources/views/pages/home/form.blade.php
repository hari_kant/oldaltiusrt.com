<div class="container altiusrt_form">
	<a name="contact" class="target"></a>
	<div class="row">
		<div class="col-md-8 col-md-offset-2  ">
			<h2>Contact Us</h2>

			@if(session('success') || \Input::get('success')) 
				<h3>Thanks for your interest in <span class="altiusrt">Altius<span>rt</span></span></h3><p>We will be in touch shortly.</p>

			@else

			<div id="form">
			{!! \Form::open(['__data-remote']) !!}
			{!! Honeypot::generate('instructions', 'objective') !!}
			  <div class="form-group {{ $errors->has('fname')?' has-error':'' }}">
				<label for="fname">First name</label>
					{{ \Form::text('fname','',['class' =>'form-control','placeholder'=>'Enter your first name']) }}
				{!! $errors->first('fname', '<span class="help-block">:message</span>') !!}

			  </div>
			  <div class="form-group {{ $errors->has('lname')?' has-error':'' }}">
				<label for="fname">Last name</label>
					{{ \Form::text('lname','',['class' =>'form-control','placeholder'=>'Enter your last name']) }}

				{!! $errors->first('lname', '<span class="help-block">:message</span>') !!}
			  </div>
			  <div class="form-group {{ $errors->has('email')?' has-error':'' }}">
				<label for="fname">Email</label>
				{{ \Form::email('email','',['class' =>'form-control','placeholder'=>'Enter your email address']) }}
				{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
			  </div>
			  <div class="form-group {{ $errors->has('organization')?' has-error':'' }}">
				<label for="organization">Organization</label>
				{{ \Form::text('organization','',['class' =>'form-control','placeholder'=>'Enter your organization name']) }}
				{!! $errors->first('organization', '<span class="help-block">:message</span>') !!}
			  </div>
			  {{--
			  <div class="form-group {{ $errors->has('title')?' has-error':'' }}">
				<label for="title">Title</label>
				<input type="text" class="form-control" id="title" name="title" placeholder="Enter your title">

				{!! $errors->first('title', '<span class="help-block">:message</span>') !!}
			  </div>
			  --}}
			  <div class="form-group {{ $errors->has('country')?' has-error':'' }}">
				<label for="country">Country</label>
				{{ \Form::text('country','',['class' =>'form-control','placeholder'=>'Enter your country']) }}
				{!! $errors->first('country', '<span class="help-block">:message</span>') !!}
			  </div>
			  <div class="form-group {{ $errors->has('message')?' has-error':'' }}">
				<label for="message">Your Message</label>
				{{ \Form::textarea('message',null,['placeholder'=>'Enter your message', 'id' => 'message', 'class' => 'form-control','rows'=>3]) }}
				{!! $errors->first('message', '<span class="help-block">:message</span>') !!}
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			{!! \Form::close() !!}
			</div>
			@endif
		</div>
	</div>
</div>