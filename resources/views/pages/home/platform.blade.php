<div class="altiusrt_platform">
	<div class="container">
		<h2>Our Platform</h2>
		<div class="row">
			<div class="col-md-6 ">

				<p>Our platform is easy to use and easy to access with best practice security and data management:</p>

				<ul>
					<li>Acesss from anywhere - cloud based web service
						<ul>
							<li>High performance data centre</li>
							<li>Optimized for low bandwidth environments: wifi, mobile tethering</li>
						</ul>
					</li>
						
					<li>Any device - cross platform, mobile and tablet friendly
						<ul>
							<li>Responsive design adapts to all screen sizes</li>
							<li>Entry screens optimized for mobile devices</li>

						</ul>
					
					</li>
					<li>Security - built in access control
						<ul>
							<li>Individual user logins</li>
							<li>Sport specific access levels and data protection</li>
							<li>Full history of data changes</li>
						</ul>
					
					</li>
					<li>Backups - keep your data safe
						<ul>
							<li>Database replication ensures data is backed up continuously</li>
							<li>Full system backups daily</li>
						</ul>
					</li>
					<li>Clean and easy user experience</li>

				</ul>
			</div>
			<div class="col-md-6 ">
				<img src="/images/LSipad2_sm.jpg" class="img-responsive" alt="">
			
			</div>
		</div>
	</div>
</div>
