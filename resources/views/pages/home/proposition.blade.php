<div class="container altiusrt_proposition">
	<div class="row">
        <div class="col-md-4 col-sm-4">
			<img src="/images/realtime.gif" border="0" alt="realtime">
			<h2>realtime</h2>
			<p><b>instantaneously capturing official stats</b> from the source: anywhere, on any device. From the field to the world
			</p>




        </div>
        <div class="col-md-4 col-sm-4">
			<img src="/images/efficiency.gif" border="0" alt="efficiency">
			<h2>efficiency</h2>
			<p><b>saving hours of setup</b> time for running tournaments, from schedules to officials to match sheets. Keeping accuracy paramount by being 100% sport specific 
		
			</p>
        </div>
        <div class="col-md-4 col-sm-4">
			<img src="/images/exposure.gif"  border="0" alt="exposure">
			<h2>exposure</h2>
			<p><b>displaying stats</b> fans want to see by combining live stats with aggregated data to turn athletes into stars and teams into dynasties
			</p>
        </div>
      </div>
   </div>
</div>