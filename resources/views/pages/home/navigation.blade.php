<nav class="navbar navbar-default navbar-fixed-top " role="navigation">
  <div class="container">
	<div class="navbar-header">
	  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </button>
  	<a class="navbar-brand hash" href="/#top"><img id="logo" src="/images/altiusrt_logo.png" ></a>
	</div>
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
		<li><a href="/#product" data-toggle="_collapse" data-target="#bs-example-navbar-collapse-1" class="hash">Product</a></li>
		<li><a href="/#testimonials" data-toggle="_collapse" data-target=".navbar-collapse"  class="hash">Testimonials</a></li>
		<li><a href="/#about" class="hash">About</a></li>
		<li><a href="/#contact" data-toggle="_collapse" data-target=".navbar-collapse" class="hash">Contact Us</a></li>
      </ul>
    </div>
  </div>
</nav>
