<div class="altiusrt_realtime_map">

		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-4">
					<h2>Athletes</h2>
					{{ number_format($stats['athletes']) }}

				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<h2>Matches played</h2>
					{{ number_format($stats['matches']) }}
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<h2>Goals scored</h2>
					{{ number_format($stats['goals']) }}
				</div>
			</div>
			<div class="caption">{{ $ago }}</div>
		<img src="/images/world_map.jpg" class="img-responsive" alt="World Map">
		
		</div>


</div>
