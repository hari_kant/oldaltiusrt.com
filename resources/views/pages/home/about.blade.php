<div class="altiusrt_about">
	<div class="container">
		<a name="about" class="target"></a>
				<h2>About Us</h2>
		<div class="row">
			<div class="col-md-6 ">
			<p>
			<span class="altiusrt">Altius</span> is a digital stats company, consisting of a group of people passionate about sports and providing experiences that excite fans across the world.</p>
			<p>We specialize in sports statistics, competition and record management, at source results capture and realtime display.</p>


				

			</div>
			<div class="col-md-6 ">

				<p>
					<span class="altiusrt">Altius<span>rt</span></span> is our flagship product line. We work with clients to provide the exact features needed to make realtime sports stats come alive. We are continually adding functionality to	<span class="altiusrt">Altius<span>rt</span></span> that allows users to integrate realtime stats with historical data for a rich, in-depth sport experience. 

				</p>
			</div>
		</div>
	</div>
</div>