
<div class="altiusrt_footer">
	<div class="container">
		<ul class="list-inline social">
			<li><a href="https://www.facebook.com/pages/Altius-RT/815599395157226?ref_type=bookmark" target=_blank><span>Like Us</span><i class="fa fa-facebook-square"></i></a></li>
			<li><a href="https://twitter.com/AltiusRT" target=_blank><span>Follow Us</span> <i class="fa fa-twitter"></i></a></li>
		</ul>
		<ul class="list-inline address" >
			<li>907 - 138 Princess St, Toronto ON  M5A 0B1</li>
			<li> | </li>
			<li><a href="mailto:info@altiusrt.com">info@altiusrt.com</a></li>
		</ul>
		<ul class="list-inline address" >
			<li>Copyright &copy; {{ date('Y') }} Altius</li>
			<li> | </li>
			<li><a href="/terms">Terms & Conditions</a></li>

		</ul>
		<ul class="list-inline address" >
			<li class="credit">Photography by Frank Uijlenbroek and Grant Treeby</li>
		<ul>

	</div>
</div>