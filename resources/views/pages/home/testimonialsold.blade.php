<div class="altiusrt_testimonials">


	<a name="testimonials" class="target"></a>

	<div id="carousel-example-generic1" class="carousel slide" data-ride="carousel"	>
		  <ol class="carousel-indicators">
		  @foreach($testimonials as $k => $t)
			<li data-target="#carousel-example-generic1" data-slide-to="{{ $k }}" @if($k===0) class="active" @endif ></li>

		  @endforeach

		  </ol>
		  <div class="carousel-inner">
		  @foreach($testimonials as $k=> $t)


			<div class="item  @if($k===0) active @endif ">

				<div class="large photo">
					<img src="{{ $t['img_large'] }}" >
				</div>

				<div class="small photo">
					<img src="{{ $t['img_small'] }}">
				</div>
				<div class="container">
					<div class="quote">
						{!! $t['quote'] !!}
						<p>- {{ $t['person'] }}</p>
					</div>

				</div>
			</div>
			@endforeach
		  </div>
		  {{--
		  <a class="left carousel-control" href="#carousel-example-generic1" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic1" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  --}}
	</div>
</div>

