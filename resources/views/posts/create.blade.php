@extends('layouts.default')

@section('content')
<div class="container">
	<h1> Create a post  </h1>

	{{ Form::open(['data-remote']) }}
	<div class="form-group">
		{{ Form::label('title','Title:') }}
		{{ Form::text('title',null,['class' => 'form-control']) }}
	</div>
	
	<div class="form-group">
		{{ Form::submit('Create Post',['class' => 'btn btn-default','data-confirm' => 'Are you sure?']) }}

	</div>



	{{ Form::close() }}
</div>	


@stop

