@extends('layouts.default')
@section('content')

	@include('pages.home.navigation')
	<div class="container">
		<h1>AltiusRT Payment Manager</h1>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					@if(\Session::has('error')) 
						<div class="alert alert-danger">
						  <strong>Error!</strong> {{ \Session::get('error') }}
						</div>
					@endif

					@if(\Session::has('success')) 
						<div class="alert alert-success">
						  <strong>Success!</strong> {{ \Session::get('success') }}
						</div>
					@endif
					<table class="table table-striped">
						<tr>
							<th>Item</th><td>{{ $payment->name }}</td>
						</tr>
						<tr>
							<th>Amount</th><td>{{ number_format($payment->amount,2) }} {{ $payment->currency }}</td>
						</tr>
						<tr>
							<th>Status</th><td>{{ ucwords($payment->status) }}</td>
						</tr>
						@if($payment->status=='New')
						<tr>
							<td colspan ="2">
								<form action="" method="POST">
								  <script
									src="https://checkout.stripe.com/checkout.js" class="stripe-button"
									data-key="{{ \Config::get('services.stripe.pk_live') }}"
									data-amount="{{ $payment->cents }}"
									data-name="AltiusRT"
									data-description="{{ $payment->description }}"
									data-image="/images/round_logo.png"
									data-locale="auto">
								  </script>
								</form>					
													
							
							</td>
						</tr>
						@endif
					</table>
				</div>
			</div>


	@include('pages.home.footer')




@stop