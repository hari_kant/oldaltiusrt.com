<!doctype html>
<html lang="en" itemscope itemtype="http://schema.org/Organization">
<head>
	<meta charset="UTF-8">
	<title>Altiusrt | realtime sports stats</title>

	<meta name="description" content="Altiusrt creates web applications to capture and showcase realtime sports stats around the world, allowing clients to save hours of set up time, instantaneously capture official stats and display stats fans want to see.">



	<meta property="og:title" content="Altiusrt | realtime sports stats" />
	<meta property="og:site_name" content="Altiusrt" />
	<meta property="og:url" content="http://altiusrt.com" />
	<meta property="og:description" content="Altiusrt creates web applications to capture and showcase realtime sports stats around the world, allowing clients to save hours of set up time, instantaneously capture official stats and display stats fans want to see." />


	<meta property="og:image" content="http://altiusrt.com/images/round_logo.png" />

	<meta property="og:image:width" content="200">
	<meta property="og:image:height" content="200">

	<meta property="twitter:description" content="Altiusrt creates web applications to capture and showcase realtime sports stats around the world." />
	<meta property="twitter:image" content="http://altiusrt.com/images/round_logo.png" />

	<meta itemprop="name" content="Altiusrt | realtime sports stats">
	<meta itemprop="description" content="Altiusrt creates web applications to capture and showcase realtime sports stats around the world, allowing clients to save hours of set up time, instantaneously capture official stats and display stats fans want to see.">
	<meta itemprop="image" content="http://altiusrt.com/images/round_logo.png">

	<link rel="shortcut icon" type="image/png" href="/favicon.png" />

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/css/main.css">
</head>
<body>

		@yield ('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="/js/sharrre/jquery.sharrre-1.3.4.min.js"></script>

 	

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.1/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.1/js/bootstrapValidator.min.js"></script>

<script src="/main.js"></script>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53948337-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
