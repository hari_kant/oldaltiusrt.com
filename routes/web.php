<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','PagesController@home');

Route::post('/','PagesController@form');

Route::get('test','PagesController@test');

Route::get('fih2014stats','PagesController@fih2014stats');

Route::get('terms','PagesController@terms');

Route::get('PFH/{any?}',function() {
	return Response::make('NotFound',404);
})->where('any','(.*)?');

Route::get('en/{any?}',function() {
	return Response::make('NotFound',404);
})->where('fr','(.*)?');
Route::get('PFH/{any?}',function() {
	return Response::make('NotFound',404);
})->where('any','(.*)?');



Route::get('/payment/{slug}', 'PaymentController@payment');

Route::post('/payment/{slug}', 'PaymentController@paymentPost');

