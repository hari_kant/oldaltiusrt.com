<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
		'sk_test' => 'sk_test_UHBD8X2oUiC7ldaiaW0OcT1K',
		'pk_test' => 'pk_test_ydW8DW6tMTsEbSF4E0IE4Rax',
		'sk_live' => 'sk_live_xWH3WhVvgSWnj2FZyR8tzPJU',
		'pk_live' => 'pk_live_mxHOiECUFV8Kxa1Ms05XaeBJ',

    ],

];
